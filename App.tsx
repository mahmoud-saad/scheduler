import {SafeAreaView} from 'react-native';
import React from 'react';
import {TimelineCalendar} from '@howljs/calendar-kit';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {exampleEvents} from './CalendarFeatures/Events';
import {
  selectedEvent,
  setSelectedEvent,
  styles,
  _onLongPressEvent,
  _onPressCancel,
  _onPressSubmit,
  _renderEditFooter,
} from './CalendarFeatures/DragToEdit';
import {isLoading, _onDateChanged} from './CalendarFeatures/FetchEvents';
import {_onDragCreateEnd} from './CalendarFeatures/DragToCreate';

const Calendar = () => {
  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <TimelineCalendar
          viewMode="week"
          theme={dayStyle}
          // Load events
          events={exampleEvents}
          // Zoom calendar
          allowPinchToZoom={true}
          initialTimeIntervalHeight={60}
          minTimeIntervalHeight={29}
          maxTimeIntervalHeight={110}
          // Create event
          allowDragToCreate={true}
          onDragCreateEnd={_onDragCreateEnd}
          dragCreateInterval={120}
          dragStep={20}
          /* theme={{
            dragCreateItemBackgroundColor: 'rgba(0, 18, 83, 0.2)',
          }} */
          // Edit event
          onLongPressEvent={_onLongPressEvent}
          selectedEvent={selectedEvent}
          onEndDragSelectedEvent={setSelectedEvent}
          // Fetching events
          /* isLoading={isLoading} */
          onDateChanged={_onDateChanged}
        />
        {!!selectedEvent && _renderEditFooter()}
      </SafeAreaView>
    </GestureHandlerRootView>
  );
};

export default Calendar;

const dayStyle = {
  //Saturday style
  saturdayName: {color: 'blue'},
  saturdayNumber: {color: 'blue'},
  saturdayNumberContainer: {backgroundColor: 'white'},

  //Sunday style
  sundayName: {color: 'red'},
  sundayNumber: {color: 'red'},
  sundayNumberContainer: {backgroundColor: 'white'},

  //Today style
  todayName: {color: 'green'},
  todayNumber: {color: 'white'},
  todayNumberContainer: {backgroundColor: 'green'},

  //Normal style
  dayName: {color: 'black'},
  dayNumber: {color: 'black'},
  dayNumberContainer: {backgroundColor: 'white'},
};
