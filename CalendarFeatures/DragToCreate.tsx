import {EventItem, RangeTime} from '@howljs/calendar-kit';
import {useState} from 'react';

const [events, setEvents] = useState<EventItem[]>([]);

export const _onDragCreateEnd = (event: RangeTime) => {
  const randomId = Math.random().toString(36).slice(2, 10);
  const newEvent = {
    id: randomId,
    title: randomId,
    start: event.start,
    end: event.end,
    color: '#A3C7D6',
  };
  setEvents(prev => [...prev, newEvent]);
};
