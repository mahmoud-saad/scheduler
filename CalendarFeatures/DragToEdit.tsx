import {EventItem, PackedEvent} from '@howljs/calendar-kit';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {exampleEvents} from './Events';

const [events, setEvents] = useState<EventItem[]>(exampleEvents);
export const [selectedEvent, setSelectedEvent] = useState<PackedEvent>();

export const _onLongPressEvent = (event: PackedEvent) => {
  setSelectedEvent(event);
};

export const _onPressCancel = () => {
  setSelectedEvent(undefined);
};

export const _onPressSubmit = () => {
  setEvents(prevEvents =>
    prevEvents.map(ev => {
      if (ev.id === selectedEvent?.id) {
        return {...ev, ...selectedEvent};
      }
      return ev;
    }),
  );
  setSelectedEvent(undefined);
};

export const _renderEditFooter = () => {
  return (
    <View style={styles.footer}>
      <TouchableOpacity style={styles.button} onPress={_onPressCancel}>
        <Text style={styles.btnText}>Cancel</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={_onPressSubmit}>
        <Text style={styles.btnText}>Save</Text>
      </TouchableOpacity>
    </View>
  );
};

export const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#FFF'},
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    height: 85,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    height: 45,
    paddingHorizontal: 24,
    backgroundColor: '#1973E7',
    justifyContent: 'center',
    borderRadius: 24,
    marginHorizontal: 8,
    marginVertical: 8,
  },
  btnText: {fontSize: 16, color: '#FFF', fontWeight: 'bold'},
});
