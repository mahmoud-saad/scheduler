import {EventItem} from '@howljs/calendar-kit';

export const exampleEvents: EventItem[] = [
  {
    id: '1',
    title: 'Event 1',
    start: '2023-02-28T09:00:05.313Z',
    end: '2023-02-28T12:00:05.313Z',
    color: '#A3C7D6',
    description: 'Hello world',
  },
  {
    id: '2',
    title: 'Event 2',
    start: '2023-02-28T11:00:05.313Z',
    end: '2023-02-28T14:00:05.313Z',
    color: '#B1AFFF',
    imageUrl: 'https://picsum.photos/200/300',
  },
];
