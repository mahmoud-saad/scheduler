import {EventItem} from '@howljs/calendar-kit';
import {useEffect, useState} from 'react';

const [events, setEvents] = useState<EventItem[]>([]);
export const [isLoading, setIsLoading] = useState(true);

const fetchData = async (props: {from: string; to: string}) => {
  return new Promise<EventItem[]>(resolve => {
    setTimeout(() => {
      console.log(props);
      resolve([]);
    }, 1000);
  });
};

useEffect(() => {
  const numOfDays = 7;
  const fromDate = new Date();
  const toDate = new Date();
  toDate.setDate(new Date().getDate() + numOfDays);
  (async () => {
    const res = await fetchData({
      from: fromDate.toISOString(),
      to: toDate.toISOString(),
    });
    setEvents(prev => [...prev, ...res]);
    setIsLoading(false);
  })();
}, []);

export const _onDateChanged = (date: string) => {
  setIsLoading(true);
  const numOfDays = 7;
  const fromDate = new Date(date);
  const toDate = new Date(date);
  toDate.setDate(toDate.getDate() + numOfDays);
  (async () => {
    const res = await fetchData({
      from: fromDate.toISOString(),
      to: toDate.toISOString(),
    });
    setEvents(prev => [...prev, ...res]);
    setIsLoading(false);
  })();
};
